package SumOfOdd;

import java.util.Scanner;

public class SumOfOddDigits {
	public static void main(String[] args) {
		int n = checkSum();
		if (n == 1) {
			System.out.println("Sum of odd digits is Odd");
		} else {
			System.out.println("Sum of odd digits is Even");
		}
	}

	public static int checkSum() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number:");
		int num = sc.nextInt();
		int digit, count = 0;
		while (num != 0) {
			digit = num % 10;
			if (digit % 2 != 0) {
				count++;

			}
			num = num / 10;
			sc.close();
		}
		if (count % 2 != 0) {
			return 1;
		} else {
			return -1;

		}

	}

}
