package Squares;

import java.util.Scanner;

public class SumOfSquares {
	public static void main(String[] args) {
		int sumOfEven = sumOfSquaresOfEvenDigits();
		System.out.println("The sum of Even Digits in given number:" + sumOfEven);

	}

	public static int sumOfSquaresOfEvenDigits() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number: ");
		int num = sc.nextInt();
		int digit;
		int sum = 0;
		while (num != 0) {
			digit = num % 10;
			if (digit % 2 == 0) {
				sum = sum + (digit * digit);
			}
			num = num / 10;
		}
		return sum;
	}

}
