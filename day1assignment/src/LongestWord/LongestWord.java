package LongestWord;

import java.util.Scanner;

public class LongestWord {
	public static void main(String[] args) {
		String largeWord = getLargestWord();
		System.out.println("The Largest Word is: " + largeWord);

	}

	public static String getLargestWord() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the string:");
		String string = scanner.nextLine();
		String word = "", large = "";
		String[] words = new String[100];
		int length = 0;
		string = string + " ";
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) != ' ') {
				word = word + string.charAt(i);
			} else {
				words[length] = word;
				length++;
				word = "";

			}
		}
		for (int k = 0; k < length; k++) {
			if (large.length() < words[k].length()) {
				large = words[k];

			}
		}
		return large;
	}
}
