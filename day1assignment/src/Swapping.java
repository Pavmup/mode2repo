
public class Swapping {
	public static void main(String[] arg){
		int temp;
		int num1 = 56;
		int num2 = 46;
		temp = num1;
		num1 = num2;
		num2 = temp;
		System.out.println("num1 = " + num1);
		System.out.println("num2 = " + num2);
	}

}
