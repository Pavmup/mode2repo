
public class RoomMain {
	public static void main(String[] args){
		Room room = new Room();
		Room.roomNo         =2345;
		Room.roomType       ="rectangle";
		Room.roomArea       =4532f;
		Room.roomAcmachine  =78;
		System.out.println("Room number      :"   +Room.roomNo);
		System.out.println("Room type        :"   +Room.roomType);
		System.out.println("Room area        :"   +Room.roomArea);
		System.out.println("Room acmachine   :"   +Room.roomAcmachine);
				
	}

}
