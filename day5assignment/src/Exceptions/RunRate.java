package Exceptions;
import java.util.Scanner;
public class RunRate {
	Scanner scan = new Scanner (System.in);
	int runs,overs;
	float runRate;
	public void input()
	{
		try{
			System.out.println("enter the total runs scored");
			runs = scan.nextInt();
			System.out.println("enter the total overs faced");
			overs = scan.nextInt();
			}
		    catch(NumberFormatException e)
		{
		    	System.out.println("enter code"+e);
		    	System.exit(0);
		}
	}
	public void compute()
	{
		runRate = runs/overs;
		System.out.println("current run rate:" + runRate);
	}

}
