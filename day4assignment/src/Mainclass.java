
public class Mainclass {


	    public static void main(String[] args) { 

	        Firstclass objA = new Firstclass(); 

	        Secondclass objB = new Secondclass(); 

	        System.out.println("in main(): "); 

	        System.out.println("objA.a = "+objA.getFirstClass()); 

	        System.out.println("objB.b = "+objB.getSecondClass()); 

	        objA.setFirstClass (222); 

	        objB.setSecondClass (333.33); 

	        System.out.println("objA.a = "+objA.getFirstClass()); 

	        System.out.println("objB.b = "+objB.getSecondClass()); 

	    } 

	} 

