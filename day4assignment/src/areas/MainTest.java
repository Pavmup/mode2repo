package areas;

public class MainTest {
	public static void main(String[] args) {
		double width = 5;
		double length = 7;
		Shape rectangle = new Rectangle(width, length);
		System.out.println("Rectangle width:" + "and length:" + length + "nResulting area :" + rectangle.area()
				+ "\nResulting perimeter:" + rectangle.perimeter() + "\n");

		double radius = 5;
		Circle circle = new Circle(radius);
		System.out.println("Circle radius:" + radius + "\nResulting Area:" + circle.area() + "\nResulting Perimeter:"
				+ circle.perimeter());

	}

}
