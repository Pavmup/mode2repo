
public class Secondclass{

	    double b = 123.45; 

	    public Secondclass() { 

	        System.out.println("-----in the constructor of class B: "); 

	        System.out.println("b = "+b); 

	        b = 3.14159; 

	        System.out.println("b = "+b); 

	    } 

	    public void setSecondClass( double value) { 

	        b = value; 

	    } 

	    public double getSecondClass() { 

	        return b; 

	    } 

	} 


