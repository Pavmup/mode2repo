package cards;

public class PayBackCard extends Card {
	private int pointsEarned;
	private double totalamount;
	
	public PayBackCard(String holderName,String cardNumber,String expiryDate) {
		super(holderName, cardNumber, expiryDate);
		
	}

	public int getPointsEarned() {
		return pointsEarned;
	}

	public void setPointsEarned(int pointsEarned) {
		this.pointsEarned = pointsEarned;
	}

	public double getTotalamount() {
		return totalamount;
	}

	public void setTotalamount(double totalamount) {
		this.totalamount = totalamount;
	}

	public PayBackCard(String holderName,String string,String string2, int pointsEarned,double  totalAmount) {
		super(holderName, string,string2);
		this.pointsEarned = pointsEarned;
		this.totalamount = totalamount;
	}
	

}
