 public class FirstLast { 
    public static int areCornerEqual(String s) 
    { 
        int n = s.length(); 
        if (n < 2) 
           return -1; 
        if (s.charAt(0) == s.charAt(n-1)) 
           return 1; 
        else
           return 0; 
    } 
  
    
    public static void main(String[] args) 
    { 
        String s = "this"; 
        int res = areCornerEqual(s); 
        if (res == -1) 
            System.out.println("Invalid Input"); 
        else if (res == 1) 
            System.out.println("valid"); 
        else
            System.out.println("not valid"); 
    } 
} 