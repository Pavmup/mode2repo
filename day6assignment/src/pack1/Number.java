package pack1;

/*Write a program to create a class Number which implements Runnable.
 * Run method displays the multiples of a number accepted as a parameter. 
 * In main create three objects - first object should display the multiples of 2, 
 * second should display the multiples of 5 and third should display the multiples of 8.
 * Display appropriate message at the beginning and ending of thread. 
 * The main thread should wait for the first object to complete.
 * Display the status of threads before the multiples are displayed and after 
 * completing the multiples. */
public class Number implements Runnable {

	public static void main(String[] args) {

	}

	@Override
	public void run() {

		System.out.print(" multiples of " + n + " are: ");

	}

	/*
	 * 
	 * We can't pass parameters to the run() method. The signature tells you
	 * that (it has no parameters).
	 */

}
